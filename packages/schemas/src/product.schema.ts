import { z } from "zod";

export const ProductSchema = z.object({
    id: z.string().length(16).optional(),
    categoryId: z.string().length(16),
    sku: z.string().min(3).max(12),
    label: z.string().min(3).max(60),
    price: z.number(),
    desciption: z.string().min(0),
    synopsis: z.string().min(0),
    featuring: z.boolean().default(false),
    createdAt: z.date()
}).strict();
