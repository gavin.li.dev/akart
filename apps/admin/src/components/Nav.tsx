import React from 'react'
import { Link } from "react-router-dom";

function NavAdmin() {
    const navItems = [
        {
            label: "Dashboard",
            href: "/"
        },
        {
            label: "Category",
            href: "/category"
        },
        {
            label: "Inventory",
            href: "/inventory"
        },
        {
            label: "Order",
            href: "/order"
        },
        {
            label: "User",
            href: "/user"
        }
    ]

    return (
        <nav className="border-b border-slate-500 bg-white">
            <ul className="flex justify-start">
                {navItems.map(n => (
                <li key={n.label}>
                    <Link className="block p-4 px-8 hover:text-black" to={n.href}>{n.label}</Link>
                </li>
                ))}
            </ul>
        </nav>
    );
}

export default NavAdmin;