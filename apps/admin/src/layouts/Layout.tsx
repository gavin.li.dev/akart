import React from "react";

import NavAdmin from "../components/Nav";


function Layout({children}: {children: React.ReactElement}) {
    return (
        <>
            <NavAdmin />
            {children}
        </>
    );
}

export default Layout;