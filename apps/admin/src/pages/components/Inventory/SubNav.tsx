"use client";

import { useState } from "react";

function SubNavInventory({action}:{action: string}) {
    const [activeTab, setActiveTab] = useState('detail');
    function handleClick(selectedTabName) {
        setActiveTab(selectedTabName);
    }

    const inactiveClassNames = "bg-slate-100 p-2";
    const activeClassNames = "bg-white p-2 border-l-4 border-cyan-100 translate-x-1";

    function getClassName(tab) {
        return tab == activeTab ? activeClassNames : inactiveClassNames
    }

    return (
        <ul className="h-full bg-slate-50 py-4 flex flex-col gap-1">
            <li className={getClassName('detail')}>
                <a className="block" href="#" onClick={() => handleClick('detail')}>Detail</a>
            </li>

            {action == "edit" ? (
            <>
            <li className={getClassName('category')}>
                <a className="block" href="#" onClick={() => handleClick('category')}>Category & Tag</a>
            </li>
            <li className={getClassName('photo')}>
                <a className="block" href="#" onClick={() => handleClick('photo')}>Photo</a>
            </li>
            </>
            ) : null}
        </ul>
    );
}

export default SubNavInventory;