import { Routes, Route} from "react-router-dom";

import ListInventory from "./List";
import EditInventoryPage from "./Edit";

export default function RoutesInventory() {
    return (
        <Routes>
            <Route index element={<ListInventory />} />
            <Route path="create" element={<EditInventoryPage action="create" />} />
            <Route path="edit/:id" element={<EditInventoryPage action="edit" />} />
        </Routes>
    )
}