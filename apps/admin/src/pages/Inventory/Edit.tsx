import React from "react";
import { Link } from "react-router-dom"
// import { useMutation } from "@tanstack/react-query";
// import { productMutation } from "services/ProductService";

import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";

// import { productMutation } from "services/ProductService";

// import { getDb } from "lib/db.connection";

import SubNavInventory from "../components/Inventory/SubNav";
import { ProductSchema } from "ak-schemas";





// import { ObjectId } from "mongodb";

// const ProductSchema = z.object({
//     categoryId: z.string().regex(/^[0-9a-f]{16}$/),

//     slug: z.string().trim(),
//     sku: z.string().min(3).max(15).trim(),
//     label: z.string().min(3).max(60),
//     price: z.number(),
//     desciption: z.string().min(0),
//     synopsis: z.string().min(0),
//     featuring: z.boolean().default(false),
//     createdAt: z.date()
// }).strict();

















// type PRODUCT = {
//     categoryId: string,
//     label: string,
//     sku: string,
//     price: number,
//     description: string,
//     synopsis: string
// }

type PRODUCT_TYPE = z.infer<typeof ProductSchema>;
export default function EditInventoryPage({action}:{action:string}) {

    // return (
    //     <div>the editor page : {action}</div>
    // )
    // const mutation = await productMutation();

    // console.log(params);

    function handleCategory() {
        //load categories
        alert('load cate')
    }

    async function handleData(data: PRODUCT_TYPE) {
        // e.preventDefault();
        console.log(data);


        // const cartDb = await getDb();
        // cartDb.collection("product").insertOne({
        //     label: "waht is ehis"
        // })




        





    }

    const { register, handleSubmit } = useForm<PRODUCT_TYPE>();

    return (
        <div className="container max-w-screen-lg  m-auto">
            <div className="overflow-auto py-4">
                <Link className="button-cancel float-left" to="..">Back</Link>
            </div>

            <form onSubmit={handleSubmit(handleData)}><div className="container max-w-screen-lg mx-auto"><div className="bg-white rounded shadow-lg p-8 grid gap-4 gap-y-2 grid-cols-4">
                
                <div className="text-gray-600">
                    <SubNavInventory action={action}/>
                </div>

                <div className="col-span-3 text-sm grid grid-cols-1 gap-4">
                    
                    <div className="">
                        <label htmlFor="categoryId">Category</label>
                        <div className="h-10 bg-gray-50 flex border border-gray-200 rounded items-center mt-1">
                            <input {...register("categoryId")} name="categoryId" id="categoryId" placeholder="Category" className="px-4 appearance-none outline-none text-gray-800 w-full bg-transparent" readOnly/>
                            
                            <button onClick={handleCategory} className="cursor-pointer outline-none focus:outline-none border-l border-gray-200 transition-all text-gray-300 hover:text-blue-600">
                                <svg className="w-4 h-4 mx-2 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                                    <polyline points="18 10 12 16 6 10"></polyline>
                                </svg>
                            </button>
                        </div>
                    </div>

                    <div className="">
                        <label htmlFor="label">Product Name</label>
                        <input {...register("label")} type="text" id="label" className="input-primary" />
                        
                    </div>

                    <div className="">
                        <label htmlFor="email">SKU</label>
                        <input {...register("sku")} type="text" name="sku" id="sku" className="input-primary" />
                    </div>

                    <div className="">
                        <label htmlFor="price">Price</label>
                        <input {...register("price")} type="number" name="price" id="price" className="input-primary" />
                    </div>


                    <div className="text-right">
                        <div className="inline-flex items-end">
                            <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Submit</button>
                        </div>
                    </div>

                </div>
            </div></div></form>
        </div>
    );
}
