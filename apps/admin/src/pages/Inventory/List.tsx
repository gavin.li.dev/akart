import { Link } from "react-router-dom";

export default function ListInventory() {
    return (
        <>
        <div>
            <Link className="block px-4 py-2 border border-blue-500 rounded" to="create">Create New</Link>
        </div>
        
        <ul>
            <li>p one</li>
            <li>p two</li>
            <li>p one</li>
            <li>p one</li>
            <li>p one</li>
        </ul>
        </>
    )
}