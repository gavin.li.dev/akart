import { Routes, Route } from 'react-router-dom';

import HomePage from './pages/Home/Home';
import RoutesInventory from './pages/Inventory/Routes';
import CategoryPage from './pages/Category/Category';
import OrderPage from './pages/Order/Order';
import UserPage from './pages/User/User';

import NotFoundPage from './pages/NotFound';

function App() {
    return (
    <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/inventory/*" element={<RoutesInventory />} />
        <Route path="/category" element={<CategoryPage />} />
        <Route path="/order" element={<OrderPage />} />
        <Route path="/user" element={<UserPage />} />
        <Route path="*" element={<NotFoundPage />} />
    </Routes>
    )
}

export default App
