import * as React from 'react';
import Link from 'next/link';

function SubNav({menuItems}) {
    return (
        <ul className="invisible opacity-20 absolute left-0 top-full -translate-x-2
            transition group-hover:opacity-100 group-hover:-translate-y-1 group-hover:visible
            w-[350px] bg-white drop-shadow-2xl rounded-lg">
            {menuItems.map((menu) => (
            <li key={menu.slug} className="group/item border-gray-200 border-b last:border-b-0">
                <Link className="flex justify-between px-6 py-3" href={menu.path}>
                    <div>{menu.label}</div>
                    <div className="hidden group-hover/item:block">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={2} className="w-6 h-6 stroke-slate-300">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                        </svg>
                    </div>
                </Link>
            </li>
            ))}
        </ul>
    )
}

export function BackDropNav() {
    const menuItems = [
        {
            label: "Mech Vehicals",
            path: "/category/mech-vehicals",
            slug: "mech-vehicals"
        },
        {
            label: "Music Box",
            path: "/category/music-box",
            slug: "music-box"
        },
        {
            label: "Marble Run",
            path: "/category/marble-run",
            slug: "marble-run"
        },
        {
            label: "Solar Power",
            path: "/category/solar-power",
            slug: "solar-power"
        }
    ]

    return (
        <nav>
            <ul className="flex justify-between">
                <li className="px-8 py-6 relative group">
                    <div className="cursor-pointer">Shop</div>
                    <SubNav menuItems={menuItems}/>
                    
                </li>
                <li className="px-8 py-6 hover:text-black">
                    <Link className="block h-full text" href="/about">
                        <div className="">Company Info</div>
                    </Link>
                </li>
                <li className="px-8 py-6 hover:text-black">
                    <Link className="block h-full" href="/about">
                        <div>Lightning Payment</div>
                    </Link>
                </li>
                <li className="px-8 py-6 hover:text-black">
                    <Link className="block h-full" href="/about">
                        <div>Contacts</div>
                    </Link>
                </li>
            </ul>
        </nav>
    );
}
