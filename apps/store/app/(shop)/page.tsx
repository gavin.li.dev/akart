import Link from 'next/link';
import * as React from 'react';
import { SimpleBanner } from 'app/components/Banner/Simple';

function CartPage() {
    const featuringProducts = [
        {slug:"p-one", name: "p one"},
        {slug:"p-two", name: "p two"},
        {slug:"p-three", name: "p three"},
        {slug:"p-four", name: "p four"}
    ]
    return (
        <section className="py-4">
            <SimpleBanner />

            <div className="grid grid-cols-4 gap-4 mx-4 h-[240px]">
                {featuringProducts.map((p) => (
                
                    <Link className="border border-slate-400" href={`/product/${p.slug}`}>
                        <span>{p.name}</span>
                    </Link>
                
                ))}
            </div>
        </section>
    );
}

export default CartPage;