import BuyButton from "components/Button/Buy";

function CategoryPage({ params }) {
    const products = [
        {
            id: "1234",
            slug: "",
            sku: "",
            label: "Marble Run Toy Craft Model Building Set",
            price: "48.8",
            featuring: false,
            thumb: "https://i.ebayimg.com/images/g/H6IAAOSwJ9Rfm3k1/s-l1600.jpg"

        },
        {
            id: "1234",
            slug: "",
            sku: "",
            label: "Marble Run Toy Craft Model Building Set",
            price: "48.8",
            featuring: false,
            thumb: "https://i.ebayimg.com/images/g/H6IAAOSwJ9Rfm3k1/s-l1600.jpg"

        },
        {
            id: "1234",
            slug: "",
            sku: "",
            label: "Marble Run Toy Craft Model Building Set",
            price: "48.8",
            featuring: false,
            thumb: "https://i.ebayimg.com/images/g/H6IAAOSwJ9Rfm3k1/s-l1600.jpg"

        },
    ]

    return (
        <section className="container m-auto">
            Category Page: {params.slug}

            {products.map((p) => (
            <div className="border border-yellow-400 rounded-lg overflow-hidden mb-8 flex ">
                <img className="block w-[400px] h-[400px]" src={p.thumb} />
                <div className="p-8">
                    <h2>{p.label}</h2>

                    <BuyButton pid={p.id} />
                </div>
            </div>
            ))}

            
        </section>
    );
}

export default CategoryPage;