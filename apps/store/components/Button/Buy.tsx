"use client"

function BuyButton({pid}) {

    const handleClick = function() {
        alert(pid);
    }

    return (
        <button onClick={handleClick} className="w-fit border border-blue-500 rounded py-4 px-12">Add To Cart</button>
    );
}

export default BuyButton;