import express from "express";
import authMW from "../mw/auth";

import { list, get } from "../controller/order.controller";

const orderRouter = express.Router();
orderRouter.get("/list", authMW, list);
orderRouter.get("/:id", authMW, get);

export default orderRouter;
