import express from "express";
import { login } from "../controller/auth.controller";

const authRouter = express.Router();
authRouter.get('/login', login);

export default authRouter;