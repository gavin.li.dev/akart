import express from "express";

import {listProduct, getProduct, createProduct, updateProduct} from "./controller/product.controller";

const productRouter = express.Router();
productRouter.get("/list", listProduct);
productRouter.get("/:id", getProduct);

export default productRouter;