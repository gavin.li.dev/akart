import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import cors from "cors";
import cookieParser from "cookie-parser";
import authRouter from "./router/auth.router";
import orderRouter from "./router/order.router";
import productRouter from "./route";

dotenv.config();

const app: Express = express();
const port = process.env.PORT || 8080;


const options: cors.CorsOptions = {
    origin: 'http://localhost:3000,http://localhost:3001'
}
app.use(cookieParser(), cors(options));




app.get('/', (req: Request, res: Response) => {
    res.send('Express + TypeScript Server');
});

app.use('/auth', authRouter);
app.use('/product', productRouter);
app.use('/order', orderRouter);


app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at https://localhost:${port}`);
});