import { Db, MongoClient } from "mongodb";

let dbClient:MongoClient;
let db:Db;

function getClient() {
    if(dbClient) {
        return dbClient;
    }

    const uri = process.env.DATABASE_URL;
    if(!uri) {
        throw new Error("Database connection url required!");
    }

    dbClient = new MongoClient(uri);
    return dbClient;
}

function getDb() {
    if(db) {
        return db;
    }

    db = getClient().db('Cart');
    return db;
}

export { getClient, getDb }