import { Request, Response } from "express";
import { getDb } from "../lib/db.connection";

function list(req: Request, res:Response) {
    return res.json({
        result: "OK",
        data: [
            {id: 123, total:456},
            {id: 124, total:456},
            {id: 125, total:456}
        ]
    })
}

async function get(req: Request, res:Response) {

    const user = res.locals.user;
    
    const db = getDb();

    const doc = await db.collection('order').insertOne({
        total: Math.random()
    });

    const orderId = req.params.id;

    if(!orderId || orderId == "") {
        return res.json({result: "Order id not found" });
    }

    // console.log('auth passed')

    return res.json({
        result: "OK",
        user: user,
        data: {id: doc.insertedId}
    })
}

export { list, get };