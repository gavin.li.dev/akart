import { Request, Response } from "express"

function login(req:Request, res:Response) {
    return res.cookie('authToken', 'bitcoinonly', { httpOnly: true })
        .json({result:"OK"})
}

export { login };