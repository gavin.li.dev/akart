import { Request, Response } from "express"

function listProduct(req: Request, res: Response) {
    return res.json({
        result: "OK",
        data: [
            {id:1, label:"one"},
            {id:2, label:"two"},
            {id:3, label:"three"},
            {id:4, label:"four"}
        ]
    })
}

function getProduct(req: Request, res: Response) {
    return res.json({
        result: "OK",
        data: {
            id:1, label:"one", detail:"this is first product"
        }
    })
}

function createProduct(req: Request, res: Response) {
    return res.json({
        result: "OK",
        data: {id:10}
    });
}

function updateProduct(req: Request, res: Response) {
    return res.json({
        result: "OK",
        data: {id:10}
    });
}

export { listProduct, getProduct, createProduct, updateProduct } 