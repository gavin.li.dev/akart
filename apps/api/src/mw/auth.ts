import { Request, Response, } from "express";

function authMW (req: Request, res: Response, next: Function) {
    const authToken = req.cookies?.authToken;

    if(authToken && authToken == "bitcoinonly") {
        res.locals.user = {id: 1234, role: 'admin'};
        return next();
    }

    return res.status(403).json({result: "auth required"});
}

export default authMW;